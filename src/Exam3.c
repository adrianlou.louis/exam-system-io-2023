#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
    FILE *fichier = NULL;
    char *mots;
    char i;
    int nombre = 0;
    fichier = fopen(argv[1], "r");

    do
    {
        i = fgetc(fichier);
        mots = strtok(i, " ");
        nombre++;
        mots = strtok(NULL, " ");
        printf("%c", i);
    } while (i != EOF);
    printf("\n");
    fclose(fichier);
    return EXIT_SUCCESS;
}