#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
    FILE *fichier = NULL;
    FILE *newfichier = NULL;
    char i;
    fichier = fopen(argv[1], "r");
    newfichier = fopen(argv[2], "w");

    do
    {
        i = fgetc(fichier);
        fputc(i, newfichier);

    } while (i != EOF);
    fclose(fichier);
    fclose(newfichier);

    return EXIT_SUCCESS;
}